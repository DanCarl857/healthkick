import { database } from './../firebase';

export function createSuggestion(suggestion) {
    return dispatch => database.ref('suggestions').push(suggestion);
}