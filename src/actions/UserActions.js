import { auth, database } from './../firebase';

export const GET_USER = 'GET_USER';
export const GET_USER_DATA = 'GET_USER_DATA';
export const GET_ALL_USERS = 'GET_ALL_USERS';

export function getUser() {
    return dispatch => {
        auth.onAuthStateChanged(user => {
            dispatch({
                type: GET_USER,
                payload: user
            });
        });
    }
};

export function getUserData(email) {
    return dispatch => {
        database.ref('users/').orderByChild("email").equalTo(email).on('value', snapshot => {
            dispatch({
                type: GET_USER_DATA,
                payload: snapshot.val()
            });
        });
    }
}

export function login(email, password) {
    return dispatch => auth.signInWithEmailAndPassword(email, password);
}

export function logout() {
    return dispatch => auth.signOut();
}

export function createAccount(email, password) {
    return dispatch => auth.createUserWithEmailAndPassword(email, password);
}

export function padUserData(user) {
    return dispatch => database.ref('users/').push(user);
}

export function getAllUsers() {
    return dispatch => database.ref('users/').on('value', snapshot => {
        var data = {
            data: snapshot.val(),
            number: snapshot.numChildren()
        };

        dispatch({
            type: GET_ALL_USERS,
            payload: data
        });
    });
}

export function updateUserData(userID, user) {
    return dispatch => database.ref('users/' + userID).set(user);
}

export function resetPasswordWithEmail(email) {
    return dispatch => auth.sendPasswordResetEmail(email);
}