import { database } from './../firebase';

export const FETCH_FOOD_BUSINESSES = 'FETCH_FOOD_BUSINESSES';
export const FETCH_SINGLE_FOOD_BUSINESS = 'FETCH_SINGLE_FOOD_BUSINESS';
export const UPDATE_FOOD_BUSINESS = 'UPDATE_FOOD_BUSINESS';
export const FETCH_MENU_ITEMS = 'FETCH_MENU_ITEMS';
export const GET_ORDERS = 'GET_ORDERS';
export const GET_FOOD_BUSINESS = 'GET_FOOD_BUSINESS';

export function getFoodBusinesses() {
    return dispatch => {
        database.ref('food-businesses/').on('value', snapshot => {
            dispatch({
                type: FETCH_FOOD_BUSINESSES,
                payload: snapshot.val()
            });
        });
    }
}

export function getBusiness(email) {
    return dispatch => {
        database.ref('food-businesses/').orderByChild("company_email").equalTo(email).on('value', snapshot => {
            dispatch({
                type: GET_FOOD_BUSINESS,
                payload: snapshot.val()
            });
        });
    }
}

export function getMenuItems(id) {
    return dispatch => {
        database.ref('food-businesses/' + id + '/menu_items').on('value', snapshot => {
            dispatch({
                type: FETCH_MENU_ITEMS,
                payload: snapshot.val()
            });
        });
    }
}

export function createFoodBusiness(foodBusiness) {
    return dispatch => database.ref('food-businesses/').push(foodBusiness);
}

export function deleteFoodBusiness(parentId, id) {
    return dispatch => database.ref('food-businesses/' + parentId + "/menu_items").child(id).remove();
}

export function getOrders(id) {
    return dispatch => {
        database.ref('food-businesses/' + id + '/orders').on('value', snapshot => {
            dispatch({
                type: GET_ORDERS,
                payload: snapshot.val()
            });
        });
    }
}

export function updateFoodBusinessWithMenuItems(id, data) {
    return dispatch => {
        database.ref('food-businesses/' + id + '/menu_items').push(data);
    }
}

export function updateFoodBusinessWithOrder(id, order) {
    return dispatch => {
        database.ref('food-businesses/' + id + '/orders').push(order);
    }
}

export function closeBusiness(id, status) {
    return dispatch => database.ref('food-businesses/' + id).update({ open: status});
}

export function updateFoodBusinessWithReviews(id, review) {
    return dispatch => {
        database.ref('food-businesses/' + id + '/reviews').push(review);
    }
}

export function placeOrder(id, order) {
    return dispatch => {
        database.ref('food-businesses/' + id + 'orders').push(order);
    }
}

export function getSingleFoodBusiness(id) {
    return dispatch => {
        database.ref('food-businesses/' + id).on('value', snapshot => {
            dispatch({
                type: FETCH_SINGLE_FOOD_BUSINESS,
                payload: snapshot.val()
            });
        });
    }
}