import { 
    FETCH_FOOD_BUSINESSES,
} from './../actions/FoodBusinessesAction';

export default function(state = [], action) {
    switch(action.type) {
        case FETCH_FOOD_BUSINESSES:
            return action.payload;
        default:
            return state;
    }
}