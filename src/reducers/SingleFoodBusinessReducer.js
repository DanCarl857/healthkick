import { GET_FOOD_BUSINESS } from './../actions/FoodBusinessesAction';

export default function(state = [], action) {
    switch(action.type) {
        case GET_FOOD_BUSINESS: 
            return action.payload;
        default: 
            return state
    }
}