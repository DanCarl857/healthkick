import { GET_ORDERS } from "../actions/FoodBusinessesAction";

export default function(state = {}, action) {
    switch(action.type) {
        case GET_ORDERS: 
            return action.payload;
        default: 
            return state;
    }
}