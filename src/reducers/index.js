import { combineReducers } from 'redux';
import FoodBusinessReducer from './FoodBusinessReducer';
import SingleFoodBusinessReducer from './SingleFoodBusinessReducer';
import ReviewsReducer from './ReviewsReducer';
import UserReducer from './UserReducer';
import UserDataReducer from './UserDataReducer';
import MenuItemsReducer from './MenuItemsReducer';
import OrdersReducer from './OrdersReducer';
import UsersReducer from './UsersReducer';

export const rootReducer = combineReducers({
    foodBusinesses: FoodBusinessReducer,
    singleFoodBusiness: SingleFoodBusinessReducer,
    reviews: ReviewsReducer,
    user: UserReducer,
    userData: UserDataReducer,
    menuItems: MenuItemsReducer,
    orders: OrdersReducer,
    users: UsersReducer
});