import { GET_USER_DATA } from './../actions/UserActions';

export default function(state = [], action) {
    switch(action.type) {
        case GET_USER_DATA:
            return action.payload;
        default: 
            return state
    }
}