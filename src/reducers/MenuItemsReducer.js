import { FETCH_MENU_ITEMS } from './../actions/FoodBusinessesAction';

export default function(state = [], action) {
    switch(action.type) {
        case FETCH_MENU_ITEMS:
            return action.payload;
        default: 
            return state
    }
}