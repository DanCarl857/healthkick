import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import './index.css';

// Import App Component
import App from './containers/App';
import Register from './containers/Register/Register';
import Login from './containers/Login/Login';
import DashBoard from './containers/Dashboard/Dashboard';
import Details from './containers/Details/Details';
import Business from './containers/Business/Business';
import FoodItem from './containers/FoodItem/FoodItem';
import Profile from './containers/Profile/Profile';
import Order from './containers/Order/Order';
import Admin from './containers/Admin/Admin';
import ForgottenPassword from './containers/ForgottenPassword/ForgottenPassword';
import Location from './containers/Location/Location';
import Terms from './containers/Terms/Terms';

// Admin section
import Orders from './containers/Admins/Orders';
import Users from './containers/Admins/Users';
import FoodBusinesses from './containers/Admins/FoodBusinesses';
import Analytics from './containers/Analytics/Analytics';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/register" component={Register} />
                <Route path="/login" component={Login} />
                <Route path="/dashboard" component={DashBoard} />
                <Route path="/details/:id" component={Details} />
                <Route path="/create-business" component={Business} />
                <Route path="/create-menu-item/:id" component={FoodItem} />
                <Route path="/profile" component={Profile} />
                <Route path="/orders" component={Order} />
                <Route path="/admin" component={Admin} />
                <Route path="/forgotten-password" component={ForgottenPassword} />
                <Route path="/location" component={Location} />
                <Route path="/terms-and-conditions" component={Terms} />

                {/* 
                    Admin section
                */}
                <Route path="/admin-order" component={Orders} />
                <Route path="/admin-users" component={Users} />
                <Route path="/admin-food-businesses" component={FoodBusinesses} />
                <Route path="/analytics" component={Analytics} />
            </Switch>
        </BrowserRouter>
    </Provider>, 
    document.getElementById('root'));