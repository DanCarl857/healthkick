//import liraries
import React, { Component } from 'react';
import './Terms.css';
import {
    Container
} from 'reactstrap';
import { Link } from 'react-router-dom';

// create a component
class Terms extends Component {
    render() {
        return (
            <Container>
                <div className="back">
                    <Link to="/">Back</Link>
                </div>
                <div className="centered">
                    <h3>Our Terms And Conditions</h3>
                </div>
                <p>
                Oat cake brownie cheesecake. Gummies biscuit ice cream. Sugar plum pie pastry halvah donut. Cupcake liquorice sesame snaps fruitcake soufflé candy canes. Halvah cake pastry halvah halvah. Ice cream danish macaroon pie cookie cotton candy wafer apple pie fruitcake. Marshmallow brownie halvah croissant bonbon pastry pudding dragée. Dragée cookie gingerbread cotton candy. Soufflé bonbon tootsie roll cookie icing powder cake. Icing chocolate cake macaroon toffee powder. Carrot cake jujubes caramels sweet roll ice cream. Sesame snaps cookie jelly-o tootsie roll.</p>

                <p>Jelly-o cake sweet marzipan soufflé tart ice cream tiramisu fruitcake. Powder tootsie roll topping gingerbread jelly beans apple pie dragée cheesecake chocolate bar. Dragée jujubes pie apple pie halvah sesame snaps jujubes. Ice cream jelly muffin powder jelly-o. Candy tart halvah bonbon. Toffee dragée oat cake tootsie roll pastry fruitcake donut cake carrot cake. Pie bonbon muffin tart pastry candy canes cake cookie. Sweet roll jelly cheesecake fruitcake donut candy canes pastry pastry liquorice. Croissant jelly candy candy macaroon toffee pie cake. Cake pastry caramels sesame snaps powder. Marzipan chocolate cake candy canes sweet roll carrot cake macaroon gingerbread candy canes. Dragée danish tart wafer caramels gingerbread jelly-o candy. Sugar plum cotton candy sugar plum sweet roll icing. Dessert topping chocolate cake chocolate bar.</p>

                <p>Carrot cake tiramisu chocolate bar chupa chups dessert cookie carrot cake macaroon. Macaroon brownie dessert. Gummi bears powder marshmallow halvah. Chocolate cake topping jelly beans caramels danish powder. Chocolate cake danish chocolate jelly-o. Marshmallow icing chocolate cake gummi bears halvah. Cheesecake tiramisu jujubes. Sesame snaps muffin pie cupcake sugar plum tart tart. Danish icing candy danish jelly-o. Halvah candy canes tart tiramisu chocolate bar marzipan lollipop sesame snaps. Muffin caramels sweet roll bear claw jelly topping jelly beans candy canes pastry. Toffee pastry lollipop bear claw danish chocolate bar cake jelly. Jelly-o muffin cotton candy halvah halvah cookie chocolate tootsie roll.</p>

                <p>Muffin halvah pudding marshmallow pie halvah muffin. Tart sweet marshmallow pudding. Cookie candy pastry muffin brownie jujubes. Candy liquorice pie chocolate cake topping gingerbread. Brownie tart tart chocolate cookie cheesecake tiramisu tart. Gingerbread liquorice soufflé tart. Gingerbread croissant oat cake lemon drops candy canes marshmallow. Croissant powder powder candy canes pastry jelly beans. Dessert dragée icing dessert chocolate bar dessert jujubes marzipan marzipan. Liquorice chupa chups fruitcake cookie. Sugar plum pastry topping fruitcake. Cookie danish danish.</p>

                <p>Tootsie roll bear claw icing cheesecake croissant chupa chups jujubes caramels. Pie croissant carrot cake cookie jelly-o brownie gummies. Cake marshmallow donut cookie cheesecake. Tiramisu tootsie roll cake apple pie. Powder cotton candy icing dragée oat cake icing gummi bears. Sweet roll danish halvah chocolate tiramisu marshmallow marshmallow oat cake pie. Jelly beans donut candy canes fruitcake. Oat cake toffee cake tiramisu marshmallow. Caramels caramels fruitcake carrot cake. Soufflé gummi bears jelly-o gummies. Cookie dessert ice cream cupcake sesame snaps muffin marshmallow chocolate gingerbread. Jujubes chocolate cupcake pastry. Marzipan toffee sugar plum biscuit lemon drops apple pie. Apple pie gingerbread liquorice gingerbread fruitcake apple pie.</p>
            </Container>
        );
    }
}


//make this component available to the app
export default Terms;
