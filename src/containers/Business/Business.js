//import liraries
import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Label,
    Input,
    Form, FormGroup
} from 'reactstrap';
import { connect } from 'react-redux';
import _ from 'lodash';
import './Business.css';

import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';
import Footer from './../../components/Footer';

import { createFoodBusiness } from './../../actions/FoodBusinessesAction';

// create a component
class Business extends Component {

    constructor(props) {
        super(props);

        this.state = {
            company_name: '',
            display_name: '',
            company_email: '',
            location: '',
            password1: '',
            password2: '',
            facebook_link: '',
            twitter_link: '',
            other_links: '',
            description: '',
            menu_items: [],
            orders: [],
            reviews: [],
            isAdmin: false,
            visible: false, 
            error: false,
            authError: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const { user_data } = this.props;
        const dbKey = Object.keys(user_data);
        var elem = dbKey[0];
        if(_.has(user_data[elem], 'isAdmin')) {
            var value = user_data[elem]['isAdmin'];
            this.setState({ isAdmin: value });
        }
    }

    onChange(event) { this.setState({ [event.target.name]: event.target.value }); }

    onSubmit(event) {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { company_email, company_name, password1, password2, location, description, display_name } = this.state;

        if (company_email === '' || company_name === '' || password1 === '' || password2 === '' || location === '' || description === '') {
            this.setState({ visible: false, error: true });
            return;
        }

        delete this.state.password1;
        delete this.state.password2;

        this.props.createFoodBusiness(this.state).then(() => {
            this.setState({ visible: false });
        }).catch(err => {
            this.setState({ visible: false, authError: true });
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.isAdmin ? <BusinessHeader /> : <UserHeader />
                }
                <Container className="marginTop70" style={{ marginBottom: 110 }}>
                    <h3>Create a Food Business...</h3><br />
                    <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                        <Row>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="company_name">Company Name&nbsp;<span className="errMsg">*</span></Label>
                                    <Input 
                                        type="text" 
                                        name="company_name" 
                                        id="company_name" 
                                        placeholder="Enter company name..."
                                        value={this.state.company_name}
                                        onChange={this.onChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="display_name">Display Name</Label>
                                    <Input 
                                        type="text" 
                                        name="display_name" 
                                        id="display_name" 
                                        placeholder="Enter company display name..."
                                        value={this.state.display_name}
                                        onChange={this.onChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label htmlFor="company_email">Company Email&nbsp;<span className="errMsg">*</span></Label>
                            <Input 
                                type="email" 
                                name="company_email" 
                                id="company_email" 
                                placeholder="Enter company email..." 
                                value={this.state.company_email}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="description">Description&nbsp;<span className="errMsg">*</span></Label>
                            <Input 
                                style={{ borderRadius: 0, borderColor: '#646464', borderWidth: 1 }}
                                type="textarea" 
                                name="description" 
                                id="description" 
                                placeholder="Enter company description..." 
                                value={this.state.description}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        <Row>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="password1">Password&nbsp;<span className="errMsg">*</span></Label>
                                    <Input 
                                        type="password" 
                                        name="password1" 
                                        id="password1" 
                                        placeholder="Enter password..." 
                                        value={this.state.password1}
                                        onChange={this.onChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="password2">Confirm Password&nbsp;<span className="errMsg">*</span></Label>
                                    <Input 
                                        type="password" 
                                        name="password2" 
                                        id="password2" 
                                        placeholder="Confirm password..." 
                                        value={this.state.password2}
                                        onChange={this.onChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label htmlFor="location">Location&nbsp;<span className="errMsg">*</span></Label>
                            <Input 
                                type="text" 
                                name="location" 
                                id="location" 
                                placeholder="Enter location..." 
                                value={this.state.company_email}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        <Row>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="facebook-link">Facebook Link</Label>
                                    <Input 
                                        type="text" 
                                        name="facebook_link" 
                                        id="facebook_link" 
                                        placeholder="Facebook url..." 
                                        value={this.state.facebook_link}
                                        onChange={this.onChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="twitter-link">Twitter Link</Label>
                                    <Input 
                                        type="text" 
                                        name="twitter_link" 
                                        id="twitter_link" 
                                        placeholder="Twitter url..." 
                                        value={this.state.twitter_link}
                                        onChange={this.onChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label htmlFor="other_links-link">Other Links</Label>
                            <Input 
                                type="text" 
                                name="other_links" 
                                id="other_links" 
                                placeholder="Other urls..." 
                                value={this.state.other_links}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        {
                            this.state.error ? <p className="errMsg">Please fill in all required information</p> : <p></p>
                        }
                        {
                            this.state.authError ? <p className="errMsg">Error creating food business. Verify you internet connection and try again</p> : <p></p>
                        }
                        <Row className="testStyle">
                            <button className="submit-button">Create Business</button>
                        </Row>
                    </Form>
                </Container>
                <Footer />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user_data: state.userData
    };
}

//make this component available to the app
export default connect(mapStateToProps, { createFoodBusiness })(Business);
