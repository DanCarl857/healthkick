import React, { Component } from 'react';
import './Home.css';
import {
    Row,
    Col,
    Container,
    Input,
} from 'reactstrap';
import { Link } from 'react-router-dom';

import Footer from './../../components/Footer';

class Home extends Component {
    render() {
        return (
            <div>
                <div className="jumbotron mainBg">
                    <Container>
                        <Row>
                            <Col xs="12" md="6" className="no-link">
                                HEALTHKICK
                            </Col>
                            <Col xs="12" md="6">
                                <Row className="float-right">
                                    <Col xs="5" md="4" className="custom-link">
                                        <Link to="/login" style={{ color: '#fff' }}>Login</Link>
                                    </Col>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{' '}
                                    <Col xs="5" md="4" className="custom-link">
                                        <Link to="/register" style={{ color: '#fff' }}>Register</Link>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                    <Container>
                        <Row className="jumbotron-header">
                            <h1 className="main-header">Experience freedom with fun</h1><br /><br />
                            <div className="sub-header">All Allergies end after your first click</div>
                        </Row>
                    </Container>
                    <Container className="main-form">
                        <Input type="text" placeholder="Enter restaurant name..." />
                        <Link to="/register"><button className="main-button">Get Going!</button></Link>
                    </Container>
                </div>

                <div>
                    <Container>
                        <Row className="works-header">
                            <h2 className="works-head">What is HealthKick?</h2>
                            <p className="works-story">
                                Biscuit tootsie roll gingerbread gummies wafer topping donut. Toffee tiramisu carrot cake croissant. Lemon drops liquorice bear claw cake chocolate bar cookie ice cream danish lemon drops. Powder halvah ice cream. Cake gummi bears cheesecake lemon drops powder bonbon soufflé brownie. Pie icing lemon drops sweet oat cake donut sweet roll macaroon. Marzipan cupcake candy marshmallow liquorice. Icing croissant gummies. Dragée marzipan icing pudding chocolate cake bonbon. Marshmallow muffin lollipop macaroon.

                                Cookie ice cream macaroon jelly-o jujubes cupcake powder.
                            </p>
                        </Row>
                    </Container>
                </div>

                <div className="feedback-section">
                    <Container>
                        <Row className="quotes-img">
                            <img src={require('./../../assets/quotes.png')} alt="quotes" />
                        </Row>
                        <Row>
                            <Container>
                                <p className="feedback">
                                    If you are allergic, you are definitely on the right platform. I really enjoyed using this platform. Would definitely recommend.
                                </p>
                            </Container>
                        </Row>
                    </Container>
                </div>

                <div className="enough-section">
                   <Container>
                       <Row>
                            <h2 className="works-head">Seen Enough?</h2>
                            <Row>
                                <Col xs="12" md="7" lg="7">
                                    <p className="works-story">
                                        Biscuit tootsie roll gingerbread gummies wafer topping donut. Toffee tiramisu carrot cake croissant. Lemon drops liquorice bear claw cake chocolate bar cookie ice cream danish lemon drops. Powder halvah ice cream. Cake gummi bears cheesecake lemon drops powder bonbon soufflé brownie. 
                                    </p>
                                </Col>
                                <Col xs="12" md="5" lg="5">
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <Link to="/register">
                                                <button className="create-button">Register</button>
                                            </Link>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <Link to="/login">
                                                <button className="contact-button">Login</button>
                                            </Link>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                       </Row>
                   </Container>
                </div>

                <div>
                    <Footer />
                </div>
            </div>
        )
    }
}

export default Home;