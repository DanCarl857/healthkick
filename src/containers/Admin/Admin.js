//import liraries
import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Modal, ModalBody, ModalHeader, ModalFooter,
    Form, FormGroup, Label,
    Input,
    Button
} from 'reactstrap';
import './Admin.css';

import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';
import { Link } from 'react-router-dom';

// create a component
class Admin extends Component {

    constructor(props) {
        super(props);

        this.state  = {
            modal: false,
            adminModal: false,
            company_name: '',
            display_name: '',
            company_email: '',
            location: '',
            password1: '',
            password2: '',
            facebook_link: '',
            twitter_link: '',
            other_links: '',
            description: '',
            menu_items: [],
            orders: [],
            reviews: [],
            isAdmin: false,
            visible: false, 
            error: false,
            authError: false,
            name: '',
            username: '',
            email: '',
            age: '',
        }

        this.toggle = this.toggle.bind(this);
        this.admintoggle = this.admintoggle.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    toggle() { this.setState({ modal: !this.state.modal }); }

    admintoggle() { this.setState({ adminModal: !this.state.adminModal }); }

    onChange(event) { this.setState({ [event.target.name]: event.target.value }); }

    onSubmit(event) {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { company_email, company_name, password1, password2, location, description, display_name } = this.state;

        if (company_email === '' || company_name === '' || password1 === '' || password2 === '' || location === '' || description === '') {
            this.setState({ visible: false, error: true });
            return;
        }

        this.props.createFoodBusiness(this.state).then(() => {
            this.setState({ visible: false });
        }).catch(err => {
            this.setState({ visible: false, authError: true });
        });
    }

    render() {
        const {
            username,
            email,
            password1,
            password2,
            error,
            name,
            age
        } = this.state;

        return (
            <div>
                <BusinessHeader />
                <Container>
                <Row className="rowp">
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/admin-order">
                            <img width="50" height="50" src={require('./../../assets/admin_orders.png')} />
                            <p className="admin-text">Orders</p>
                        </Link>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/admin-users">
                            <img src={require('./../../assets/admin_users.png')} />
                            <p className="admin-text">Users</p>
                        </Link>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/analytics">
                            <img src={require('./../../assets/admin_stats.png')} />
                            <p className="admin-text">Analytics</p>
                        </Link>
                    </Col>
                    
                </Row>
                <Row className="rowp">
                    <Col xs="12" md="3" lg="3" className="box">
                        <img width="50" height="50" src={require('./../../assets/time_delivery.png')} />
                        <p className="admin-text">Average Time</p>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/admin-food-businesses">
                            <img width="50" height="50" src={require('./../../assets/veg.png')} />
                            <p className="admin-text">Food Businesses</p>
                        </Link>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box" onClick={this.toggle}>
                        <Link to="/create-business">
                            <img width="50" height="50" src={require('./../../assets/add.png')} />
                            <p className="admin-text">Add Food Business</p>
                        </Link>
                    </Col>
                </Row>
                <Row className="rowp">
                    <Col xs="12" md="3" lg="3" className="box" onClick={this.admintoggle}>
                        <img width="50" height="50" src={require('./../../assets/profile.png')} />
                        <p className="admin-text">Create Admin</p>
                    </Col>
                    <Modal isOpen={this.state.adminModal} toggle={this.admintoggle}>
                            <ModalHeader toggle={this.admintoggle}>Create an Administrator</ModalHeader>
                            <ModalBody className="rev-body">
                            <Form className="indiv-reg-form" onSubmit={this.onSubmitIndividual}>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="name">Name&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="text" 
                                                    name="name" 
                                                    id="name" 
                                                    placeholder="Enter name..." 
                                                    value={name}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="username">Username&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="text" 
                                                    name="username" 
                                                    id="username" 
                                                    placeholder="Enter username..." 
                                                    value={username}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="email">Email&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="email" 
                                                    name="email" 
                                                    id="email" 
                                                    placeholder="Enter email..." 
                                                    value={email}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="username">Age&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="number" 
                                                    name="age" 
                                                    id="age" 
                                                    placeholder="Enter age..." 
                                                    value={age}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="password">Password&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="password" 
                                                    name="password1" 
                                                    id="password1" 
                                                    placeholder="Enter password..." 
                                                    value={password1}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="confirm_password">Confirm Password&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="password" 
                                                    name="password2" 
                                                    id="password2" 
                                                    placeholder="confirm password..." 
                                                    value={password2}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    {
                                        this.state.indiv_error ? <p className="errMsg">Please fill in all the required information</p> : <p></p>
                                    }
                                    {
                                        this.state.indiv_authError ? <p className="errMsg">Error creating user account. Please verify your information and try again</p> : <p></p>
                                    }
                                    <Row className="testStyle">
                                        <button className="submit-button" type="submit">Register</button>
                                    </Row>
                                </Form>
                            </ModalBody>
                            {
                                this.state.reviewError ? <p className="errMsg">Please fill in all the required information</p> : <p></p>
                            }
                            {
                                this.state.reviewAuthErr ? <p className="errMsg">Error submitting review. Please verify your internet connection and try again</p> : <p></p>
                            }
                            <ModalFooter className="rev-foot">
                                <Button color="secondary" onClick={this.admintoggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                </Row>
                </Container>
            </div>
        );
    }
}

//make this component available to the app
export default Admin;
