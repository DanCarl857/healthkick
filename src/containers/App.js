import React, { Component } from 'react';

// import components
import Home from './Home/Home';

class App extends Component {
    render() {
        return (
            <div>
                <Home />
            </div>
        )
    }
}

export default App;