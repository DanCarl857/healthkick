//import liraries
import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Input, Label,
    Form, FormGroup
} from 'reactstrap';
import { connect } from 'react-redux';
import _ from 'lodash';
import './FoodItem.css';

import { updateFoodBusinessWithMenuItems } from './../../actions/FoodBusinessesAction';

import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';

// create a component
class FoodItem extends Component {

    componentWillMount() {
        this.selectedDiets = new Set();
    }
    
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            description: '',
            diets: [],
            price: '',
            error: false,
            visible: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const { user_data } = this.props;
        const dbKey = Object.keys(user_data);
        var elem = dbKey[0];
        if(_.has(user_data[elem], 'isAdmin')) {
            var value = user_data[elem]['isAdmin'];
            this.setState({ isAdmin: value });
        }
    }

    onChange(e) {
        if (e.target.name === "diets") {
            if(this.selectedDiets.has(e.target.value)) {
                this.selectedDiets.delete(e.target.value);
            } else {
                this.selectedDiets.add(e.target.value);
            }
            this.setState({ diets: this.selectedDiets});
        } else {
            this.setState({ [e.target.name]: e.target.value });
        }
    };

    onSubmit(event) {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { name, description, diets, price } = this.state;

        if (name === '' || description === '' || diets === [] || price === '') {
            this.setState({ error: true, visible: false });
            return;
        }

        this.props.updateFoodBusinessWithMenuItems(this.props.match.params.id, this.state);
    }

    render() {
        return (
            <div>
                {
                    this.state.isAdmin ? <BusinessHeader /> : <UserHeader />
                }
                <Container className="marginTop90">
                    <h3>Create a Menu Item...</h3><br />
                    <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                        <FormGroup>
                            <Label htmlFor="name">Name&nbsp;<span className="errMsg">*</span></Label>
                            <Input type="text" name="name" id="name" value={this.state.name} onChange={this.onChange} placeholder="Enter name..." />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="description">Description&nbsp;<span className="errMsg">*</span></Label>
                            <Input type="text" name="description" id="description" value={this.state.description} onChange={this.onChange} placeholder="Enter description..." />
                        </FormGroup>
                        <br />
                        <Row>
                            <Col xs="12" md="6" lg="6">
                            <Label htmlFor="diets">Allergies supported</Label><br /><br />
                                <FormGroup check inline>
                                    <Label check>
                                        <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Lactose Intolerant"
                                            onChange={this.onChange}
                                        /> <span className="check-fix">Lactose Intolerant</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                    <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Vegans"
                                            onChange={this.onChange}
                                        />  <span className="check-fix">Vegans</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                    <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Nuts"
                                            onChange={this.onChange}
                                        />  <span className="check-fix">Nuts</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                    <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Wheat"
                                            onChange={this.onChange}
                                    />  <span className="check-fix">Wheat</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                        <Input type="checkbox" /> <span className="check-fix">Corn</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                        <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Soy"
                                            onChange={this.onChange}
                                        /> <span className="check-fix">Soy</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                        <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Garlic"
                                            onChange={this.onChange}
                                        /> <span className="check-fix">Garlic</span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check inline>
                                    <Label check>
                                        <Input 
                                            type="checkbox"
                                            name="diets" 
                                            value="Gluten"
                                            onChange={this.onChange}
                                        /> <span className="check-fix">Gluten</span>
                                    </Label>
                                </FormGroup>
                            </Col>
                            <Col xs="12" md="6" lg="6">
                                <FormGroup>
                                    <Label htmlFor="price">Price&nbsp;<span className="errMsg">*</span></Label>
                                    <Input type="number" name="price" id="price" value={this.state.price} onChange={this.onChange} placeholder="Enter price..." />
                                </FormGroup>
                            </Col>
                        </Row>
                        {
                            this.state.error ? <p className="errMsg">Please fill in all required information</p> : <p></p>
                        }
                        <Row className="testStyle">
                            <button className="submit-button">Create Menu Item</button>
                        </Row>
                    </Form>
                </Container>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user_data: state.userData
    };
}


//make this component available to the app
export default connect(mapStateToProps, { updateFoodBusinessWithMenuItems })(FoodItem);
