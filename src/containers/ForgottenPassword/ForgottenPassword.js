import React, { Component } from 'react';
import './../Login/Login.css'; // lets borrow some styling ;-)
import {
    Container,
    Row,
    Col,
    Card, 
    CardTitle, 
    CardBody,
    FormGroup, Form, Input, Label
} from 'reactstrap';
import HeaderRegistration from '../../components/HeaderRegistration';
import Footer from '../../components/Footer';
import { login, getUser, resetPasswordWithEmail } from './../../actions/UserActions';
import { connect } from 'react-redux';

class ForgottenPassword extends Component {

    componentWillMount() {
        this.props.getUser();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.email !== undefined) {
            this.props.history.push('/dashboard');
        }
    }
    
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            error: false,
            visible: false,
            authError: false,
            showAlert: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ visible: !this.state.visible });
        const { email } = this.state;

        if (email === '') {
            this.clearFields();
            this.setState({ error: true, visible: false });
            return;
        }

        new Promise(resolve => resolve(this.props.resetPasswordWithEmail(email)))
            .then(() => this.setState({ showAlert: true }));
    }

    clearFields() {
        this.setState({ email: '', error: false, authError: false });
    }

    render() {
        return (
            <div className="registration-bg">
                <HeaderRegistration question="Don't have an Account?" actionText="Signup" myLink="/register" />
                <Container className="container-register center-block">
                    <Card className="container-form">
                        <CardTitle className="centered-text register-header">Forgotten Password</CardTitle>
                        <CardBody>
                            <Row>
                                {
                                    this.state.showAlert ? 
                                    <div className="centerStyle alertStyle">
                                        Succesfully reset password. Please check your email
                                    </div> : <div></div>
                                }
                            </Row>
                            <Row>
                                <Col sm="12">
                                    <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                                        <FormGroup>
                                            <Label for="email">Email</Label>
                                            <Input 
                                                type="email" 
                                                name="email" 
                                                id="email" 
                                                value={this.state.email}
                                                onChange={this.onChange}
                                                placeholder="Enter email" 
                                            />
                                        </FormGroup>
                                        {
                                            this.state.error ? <p className="errMsg">Please fill in your email address</p> : <p></p>
                                        }
                                        {
                                            this.state.authError ? <p className="errMsg">Error logging in. Please verify your information and try again</p> : <p></p>
                                        }
                                        <Row className="testStyle">
                                            <button className="submit-button">ForgottenPassword</button>
                                        </Row>
                                    </Form>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Container>
                <div className="marginTop110">
                    <Footer />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { user: state.user };
};

export default connect(mapStateToProps, { login, getUser, resetPasswordWithEmail })(ForgottenPassword);