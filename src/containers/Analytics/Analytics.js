//import liraries
import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Modal, ModalBody, ModalHeader, ModalFooter,
    Form, FormGroup, Label,
    Input,
    Button
} from 'reactstrap';
import './Analytics.css';

import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';
import { Link } from 'react-router-dom';

// create a component
class Analytics extends Component {

    componentDidMount() {
        // TODO: 1. Get all food businesses and iterate through to get number of orders
        // TODO: 2. Get all users and count firebase style
        // TODO: 3. get time ranges and compare from 1.
        // TODO: 4. Restaurant with most orders
    }

    constructor(props) {
        super(props);

        this.state  = {
            modal: false,
            adminModal: false,
            company_name: '',
            display_name: '',
            company_email: '',
            location: '',
            password1: '',
            password2: '',
            facebook_link: '',
            twitter_link: '',
            other_links: '',
            description: '',
            menu_items: [],
            orders: [],
            reviews: [],
            isAdmin: false,
            visible: false, 
            error: false,
            authError: false,
            name: '',
            username: '',
            email: '',
            age: '',
        }

        this.toggle = this.toggle.bind(this);
        this.admintoggle = this.admintoggle.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    toggle() { this.setState({ modal: !this.state.modal }); }

    admintoggle() { this.setState({ adminModal: !this.state.adminModal }); }

    onChange(event) { this.setState({ [event.target.name]: event.target.value }); }

    onSubmit(event) {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { company_email, company_name, password1, password2, location, description, display_name } = this.state;

        if (company_email === '' || company_name === '' || password1 === '' || password2 === '' || location === '' || description === '') {
            this.setState({ visible: false, error: true });
            return;
        }

        this.props.createFoodBusiness(this.state).then(() => {
            this.setState({ visible: false });
        }).catch(err => {
            this.setState({ visible: false, authError: true });
        });
    }

    render() {
        const {
            username,
            email,
            password1,
            password2,
            error,
            name,
            age
        } = this.state;

        return (
            <div>
                <BusinessHeader />
                <Container>
                <Row className="rowp">
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/admin-order">
                            <img width="50" height="50" src={require('./../../assets/admin_orders.png')} />
                            <p className="admin-text">Number of users</p>
                            <p className="analytics-value">500</p> 
                        </Link>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/admin-users">
                            <img src={require('./../../assets/admin_users.png')} />
                            <p className="admin-text">Number of Orders</p>
                            <p className="analytics-value">500</p> 
                        </Link>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/analytics">
                            <img src={require('./../../assets/admin_stats.png')} />
                            <p className="admin-text">Percentage Increase rate</p>
                            <p className="analytics-value">50 %</p> 
                        </Link>
                    </Col>
                    
                </Row>
                <Row className="rowp">
                    <Col xs="12" md="3" lg="3" className="box">
                        <img width="50" height="50" src={require('./../../assets/time_delivery.png')} />
                        <p className="admin-text">Busiest order time range</p>
                        <p className="analytics-value">12:00 AM</p>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box">
                        <Link to="/admin-food-businesses">
                            <img width="50" height="50" src={require('./../../assets/veg.png')} />
                            <p className="admin-text">Most popular Food business</p>
                            <p className="analytics-value">IYA Restaurant</p>
                        </Link>
                    </Col>
                    <Col xs="12" md="3" lg="3" className="box" onClick={this.toggle}>
                        <Link to="/create-business">
                            <img width="50" height="50" src={require('./../../assets/profile.png')} />
                            <p className="admin-text">Most common allergy</p>
                            <p className="analytics-value">Wheat</p>
                        </Link>
                    </Col>
                </Row>
                </Container>
            </div>
        );
    }
}

//make this component available to the app
export default Analytics;
