import React, { Component } from 'react';
import {
    Container,
    Col, 
    Row,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Input,
    Button
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

// import styles
import './Dashboard.css';

// import components
import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';
import Footer from './../../components/Footer';

import { getBusiness, getFoodBusinesses } from './../../actions/FoodBusinessesAction';
import { createSuggestion } from './../../actions/SuggestionsAction';
import { getUser, getUserData } from './../../actions/UserActions';


class Dashboard extends Component {

    componentWillMount() {
        this.props.getFoodBusinesses();
        this.props.getUser();

        if (this.props.user.loading === false && this.props.user.email === undefined) {
            this.props.history.replace('/login');
        } else {
            this.props.getUserData(this.props.user.email);
            this.props.getBusiness(this.props.user.email);
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            modal: false,
            orderModal: false,
            title: '',
            suggestion: '',
            isAdmin: false,
            suggestionError: false,
            visible: false,
            link: ''
        };

        this.toggle = this.toggle.bind(this);
        this.renderFoodBusinesses = this.renderFoodBusinesses.bind(this);
        this.submitSuggestion = this.submitSuggestion.bind(this);
        this.onChange = this.onChange.bind(this);
        this.change = this.change.bind(this);
    }

    componentDidMount() {
        const { user_data } = this.props;
        const dbKey = Object.keys(user_data);
        var elem = dbKey[0];
        if(_.has(user_data[elem], 'isAdmin')) {
            var value = user_data[elem]['isAdmin'];
            this.setState({ isAdmin: value });
        }
    }

    toggle() { this.setState({ modal: !this.state.modal, suggestionError: false }); }

    clearSuggestionFields() {
        this.setState({
            title: '',
            suggestion: ''
        });
    }

    submitSuggestion(e) {
        e.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { title, suggestion } = this.state;

        const data = {
            title: title,
            suggestion: suggestion
        }

        if (title == '' || suggestion == '') {
            this.clearSuggestionFields();
            this.setState({ suggestionError: true, visible: false });
            return;
        }

        this.props.createSuggestion(data).then(() => {
            this.setState({ visible: false });
        }).catch(err => {
            this.setState({ suggestionError: true, visible: false });
        });
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    change() {
        // get logged in user id
        let key = Object.keys(this.props.business)[0];
        this.props.history.push(`/create-menu-item/${key}`);
    }

    renderFoodBusinesses() {

        return _.map(this.props.foods, (food, key) => {
            return (
                <Col xs={12} sm={12} md={3} className="food-business" key={key}>
                    <Link to={`/details/${key}`} style={{ textDecoration: 'none', color: '#3e4852' }}>
                        <img height="150" width="250" src={require('./../../assets/rest.jpeg')} alt="Restaurant 1" />
                        <p className="food-text">{food.company_name}</p>
                        <p className="reviews">
                            8.0/10 rating<span className="rev">(1200 reviews)</span>
                        </p>
                        <p>Location: {food.location}</p>
                    </Link><br /><br />
                </Col>
            )
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.isAdmin ? <BusinessHeader /> : <UserHeader />
                }
                <Container>
                    <Row className="search-filter">
                        <Col xs="12" md="5" lg="5">
                            <Input type="text" placeholder="Search by food business..." />
                            <br/>
                        </Col>
                        <Col xs="12" md="5" lg="5">
                            <Input type="text" placeholder="Search by menu items..." />
                            <br/>
                        </Col>
                        <Col xs="12" md="2" lg="2">
                            <button className="search-btn">Search</button>
                            <br/>
                        </Col>
                    </Row>
                    <Row className="food-business-container marginTop90">
                        {this.renderFoodBusinesses()}
                    </Row>
                </Container>
                <div className="fab" onClick={this.change}>+</div>
                {
                    this.state.isAdmin ? <div className="fab" onClick={this.change}> + </div> : <div></div>
                }
                <div className="fab-suggestions" onClick={this.toggle}> ? </div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal-top">
                    <ModalHeader toggle={this.toggle}>Make A Suggestion to the Administrator(s)</ModalHeader>
                    <ModalBody className="rev-body">
                        <Input type="text" name="title" value={this.state.value} onChange={this.onChange} placeholder="Enter title..." />
                        <br />
                        <Input style={{ borderRadius: 0 }} type="textarea"  name="suggestion" value={this.state.suggestion} onChange={this.onChange} placeholder="Enter your suggestion..." />
                    </ModalBody>
                    {
                        this.state.suggestionError ? <p className="errMsg">Please fill in all fields</p> : <p></p>
                    }
                    <ModalFooter className="rev-foot">
                        <Button style={{ color: '#fff', backgroundColor: '#ff7979', borderWidth: 1, borderColor: '#ff7979' }} onClick={this.submitSuggestion}>Submit</Button>{' '}
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <br/><br/>
                <div className="marginTop110">
                    <Footer />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps){
    return {
        foods: state.foodBusinesses,
        user: state.user,
        user_data: state.userData,
        business: state.singleFoodBusiness
    }
}

export default connect(mapStateToProps, { 
    getBusiness, 
    createSuggestion,
    getFoodBusinesses,
    getUser,
    getUserData
})(Dashboard);