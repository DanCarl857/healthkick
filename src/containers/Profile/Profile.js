//import liraries
import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Input,
    Label,
    Form, FormGroup
} from 'reactstrap';
import './Profile.css';
import { connect } from 'react-redux';
import _ from 'lodash';

import UserHeader from './../../components/UserHeader';

import { getUser, getUserData, updateUserData } from './../../actions/UserActions';

let initial_user_data = {}

// create a component
class Profile extends Component {

    componentWillMount() {
        this.props.getUser();
        this.selectedDiets = new Set();
    }

    componentDidMount() {
        const { user_data } = this.props;
        let values = Object.values(user_data)[0];

        if (values !== undefined) {
            // object to temporarily store user data
            initial_user_data = {
                email: values.email,
                name: values.name,
                age: values.age,
                username: values.username,
                diets: values.diets
            }

            this.setState({
                email: values.email,
                name: values.name,
                age: values.age,
                username: values.username,
                diets: values.diets
            });
        }
    }

    isChecked(diet) {
        if (initial_user_data !== undefined) {
            return _.includes(initial_user_data.diets, diet);
        } else {
            return false
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            email: '',
            age: '',
            name: '',
            diets: [],
            phone: '',
            same: false,
            visible: false,
            error: false, 
            done: false,
            showAlert: false
        }

        this.isChecked = this.isChecked.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({ visible: !this.state.visible });
        const { email, username, age, name, phone, diets } = this.state;

        if (email === '' || username === '' || age === '' || name === '' || phone === '') {
            this.resetFields();
            this.setState({ error: true, visible: false });
            return;
        }

        if (email === initial_user_data.email || username === initial_user_data.username || age === initial_user_data.age || name === initial_user_data.name || phone === initial_user_data) {
            this.setState({ same: true });
            return;
        }

        var user = { email , isAdmin: false, name, username, allergies: diets, phone, age };

        new Promise((resolve) => resolve(this.props.updateUserData(user)))
            .then(() => {
                this.setState({ done: true, showAlert: true });
            });
    }

    resetFields() {
        this.setState({ 
            email: initial_user_data.email, 
            username: initial_user_data.username,
            name: initial_user_data.name,
            age: initial_user_data.age,
            error: false, authError: false 
        });
    }

    onChange = (e) => {
        if (e.target.name === "user_diets") {
            if(this.selectedDiets.has(e.target.value)) {
                this.selectedDiets.delete(e.target.value);
            } else {
                this.selectedDiets.add(e.target.value);
            }
            this.setState({ diets: this.selectedDiets});
        } else {
            this.setState({ [e.target.name]: e.target.value });
        }
    };

    render() {
        return (
            <div>
                <UserHeader />
                <Container>
                    <Row>
                        <Col xs="12" sm="12" md="10" lg="10" className="marginTop110 box">
                        <Row>
                            {
                                this.state.showAlert ? 
                                <div className="centerStyle alertStyle">
                                    Succesfully update your profile
                                </div> : <div></div>
                            }
                        </Row>
                            <Container>
                                <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label for="name">Name</Label>
                                                <Input 
                                                    type="text" 
                                                    name="name" 
                                                    id="name" 
                                                    value={this.state.name}
                                                    onChange={this.onChange}
                                                    placeholder="Enter name..." 
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label for="username">Username</Label>
                                                <Input 
                                                    type="text" 
                                                    name="username" 
                                                    id="username" 
                                                    value={this.state.username}
                                                    onChange={this.onChange}
                                                    placeholder="Enter username..." 
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label for="name">Email</Label>
                                                <Input 
                                                    type="email" 
                                                    name="email" 
                                                    id="email" 
                                                    value={this.state.email}
                                                    onChange={this.onChange}
                                                    placeholder="Enter name..." 
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label for="username">Age</Label>
                                                <Input 
                                                    type="number" 
                                                    name="age" 
                                                    id="age" 
                                                    value={this.state.age}
                                                    onChange={this.onChange}
                                                    placeholder="Enter username..." 
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Label htmlFor="user_diets">Allergies you have</Label>
                                    <Row>
                                        <Col>
                                            <FormGroup check inline>
                                                <Label check>
                                                    <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Lactose Intolerant"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Lactose Intolerant')}
                                                    /> <span className="check-fix">Lactose Intolerant</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Vegans"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Vegans')}
                                                    />  <span className="check-fix">Vegans</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Nuts"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Nuts')}
                                                    />  <span className="check-fix">Nuts</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Wheat"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Wheat')}
                                                />  <span className="check-fix">Wheat</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                    <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Corn"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Corn')}
                                                    /> <span className="check-fix">Corn</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                    <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Soy"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Soy')}
                                                    /> <span className="check-fix">Soy</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                    <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Garlic"
                                                        checked={this.isChecked('Garlic')}
                                                        onChange={this.onChange}
                                                    /> <span className="check-fix">Garlic</span>
                                                </Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Label check>
                                                    <Input 
                                                        type="checkbox"
                                                        name="user_diets" 
                                                        value="Gluten"
                                                        onChange={this.onChange}
                                                        checked={this.isChecked('Gluten')}
                                                    /> <span className="check-fix">Gluten</span>
                                                </Label>
                                            </FormGroup>
                                        </Col>
                                    </Row><br />
                                    <FormGroup>
                                        <Label for="phone">Phone Number</Label>
                                        <Input 
                                            type="number" 
                                            name="phone" 
                                            id="phone"
                                            value={this.state.phone} 
                                            onChange={this.onChange}
                                            placeholder="Enter phone number..." 
                                        />
                                    </FormGroup>
                                    { this.state.error && <p className="errMsg">Please make sure all fields have some data</p>}
                                    { this.state.same && <p className="nothing">Nothing has been changed yet</p>}
                                    { this.state.done && <p className="nothing">Update successfull!</p>}
                                    <Row className="testStyle">
                                        <button className="submit-button" type="submit">Update Profile</button>
                                    </Row>
                                </Form>
                            </Container>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user_data: state.userData,
        user: state.user
    }
}

//make this component available to the app
export default connect(mapStateToProps, { getUserData, getUser, updateUserData })(Profile);
