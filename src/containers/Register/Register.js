import React, { Component } from 'react';
import './Register.css';
import {
    Container,
    Row,
    Col,
    TabContent,
    TabPane,
    Card, 
    CardTitle, 
    CardBody,
    Nav, NavItem, NavLink,
    FormGroup, Form, Input, Label
} from 'reactstrap';
import classnames from 'classnames';
import { connect } from 'react-redux';
import HeaderRegistration from '../../components/HeaderRegistration';
import Footer from '../../components/Footer';

import { createAccount, padUserData } from './../../actions/UserActions';
import { createFoodBusiness } from './../../actions/FoodBusinessesAction';

import firebase from 'firebase';
// import FileUploader from 'react-firebase-file-uploader';

class Register extends Component {

    componentWillMount() {
        this.selectedDiets = new Set();
    }

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            name: '',
            username: '',
            email: '',
            age: '',
            phone: '',
            password1: '',
            password2: '',
            error: null,
            activeTab: 1, 
            company_name: '',
            company_display_name: '',
            company_location: '',
            company_email: '',
            company_password1: '',
            company_password2: '',
            company_description: '',
            company_phone: '',
            twitter: '',
            other_links: '',
            facebook: '',

            user_diets: [],
            company_allergies: [],

            avatar: '',
            isUploading: false,
            progress: 0,
            avatarURL: '',
            indiv_error: false,
            indiv_authError: false,
            comp_error: false,
            comp_authError: false,
            visible: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmitIndividual = this.onSubmitIndividual.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });

    handleProgress = (progress) => this.setState({progress});

    handleUploadError = (error) => {
        this.setState({ isUploading: false });
        console.log(error);
    }

    handleUploadSuccess = (filename) => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({ avatarURL: url }));
    }

    onChange = (e) => {
        if (e.target.name === "user_diets") {
            if(this.selectedDiets.has(e.target.value)) {
                this.selectedDiets.delete(e.target.value);
            } else {
                this.selectedDiets.add(e.target.value);
            }
            this.setState({ user_diets: this.selectedDiets});
        } else if(e.target.name === "company_allergies") { // Find a way to avoid this duplication
            if(this.selectedDiets.has(e.target.value)) {
                this.selectedDiets.delete(e.target.value);
            } else {
                this.selectedDiets.add(e.target.value);
            }
            this.setState({ company_allergies: this.selectedDiets});
        } else {
            this.setState({ [e.target.name]: e.target.value });
        }
    };

    onSubmitIndividual = (event) => {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { email, password1, name, username, age, password2, user_diets, phone } = this.state;
        var diets = Array.from(user_diets);
        var user = { email, isAdmin: false, name, username, age, diets, phone };
        

        if (email === '' || password1 === '' || password2 === '' || username === '' || age === '' || name === '' || user_diets === [] || phone === '') {
            this.setState({ indiv_error: true, visible: false });
            return;
        }

        this.props.createAccount(email, password1).then(() => {
            this.props.padUserData(user).then(() => {
                this.setState({ visible: false });
                this.props.history.replace('/dashboard');
            });
        }).catch(err => {
            this.clearFields();
            this.setState({ indiv_authError: true, visible: false });
            return;
        });
    }

    clearFields() {
        this.setState({
            name: '',
            username: '',
            email: '',
            age: '',
            password1: '',
            password2: '',
            phone: '',
            error: null,
            activeTab: 1, 
            company_name: '',
            company_display_name: '',
            company_phone: '',
            company_location: '',
            company_email: '',
            company_password1: '',
            company_password2: '',
            company_description: '',
            twitter: '',
            other_links: '',
            facebook: '',
            indiv_error: false,
            indiv_authError: false,
            comp_error: false,
            comp_authError: false,
            visible: false
        });
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { 
            company_email, 
            company_display_name, 
            company_name, 
            company_password1, 
            company_password2,
            company_phone, 
            company_location,
            facebook,
            twitter,
            other_links,
            company_description,
            company_allergies
        } = this.state;

        var company = {
            company_name: company_name,
            display_name: company_display_name,
            company_email: company_email,
            company_phone,
            location: company_location,
            password1: company_password1,
            facebook_link: facebook,
            twitter_link: twitter,
            other_links: other_links,
            description: company_description,
            open: true,
            menu_items: [],
            orders: [],
            reviews: [],
            company_allergies: []
        }

        var allergies = Array.from(company_allergies);
        var user = { email: company_email , isAdmin: true, name: company_name, username: company_display_name, allergies, phone: company_phone };

        if (company_email === '' || company_password1 === '' || company_password2 === '' || company_location === '' || company_description === '' || company_name === '' || company_phone === '') {
            this.setState({ comp_error: true, visible: false });
            return;
        }

        this.props.createAccount(company_email, company_password1).then(() => {
            delete company.password1;
            this.props.createFoodBusiness(company).then(() => {
                this.props.padUserData(user).then(() => {
                    this.setState({ visible: false });
                    this.props.history.replace('/dashboard');
                });
            });
        }).catch(err => {
            this.clearFields();
            this.setState({ comp_authError: true, visible: false });
        });
    }

    componentDidMount() {
        this.toggle("1");
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {

        const {
            username,
            email,
            password1,
            password2,
            error,
            name,
            age,
            phone
        } = this.state;

        return (
            <div className="registration-bg">
                <HeaderRegistration question="Already Have an Account?" actionText="Login" myLink="/login" />
                <Container className="container-register center-block">
                    <Card className="container-form">
                        <CardTitle className="centered-text register-header">Register</CardTitle>
                        <CardBody>
                            <Nav tabs  className="nav-justified">
                                <NavItem>
                                    <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}>
                                        Individual
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}>
                                        Food Business
                                    </NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.activeTab} className="marginTop30">
                                <TabPane tabId="1">
                                    <Row>
                                        <Col sm="12">
                                            <Form className="indiv-reg-form" onSubmit={this.onSubmitIndividual}>
                                                <Row>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Name&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="text" 
                                                                name="name" 
                                                                id="name" 
                                                                placeholder="Enter name..." 
                                                                value={name}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="username">Username&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="text" 
                                                                name="username" 
                                                                id="username" 
                                                                placeholder="Enter username..." 
                                                                value={username}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col xs="12" md="12" lg="12">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Phone&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="number" 
                                                                name="phone" 
                                                                id="phone" 
                                                                placeholder="Enter phone number..." 
                                                                value={phone}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="email">Email&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="email" 
                                                                name="email" 
                                                                id="email" 
                                                                placeholder="Enter email..." 
                                                                value={email}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="username">Age&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="number" 
                                                                name="age" 
                                                                id="age" 
                                                                placeholder="Enter age..." 
                                                                value={age}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Label htmlFor="user_diets">Allergies you have</Label>
                                                <Row>
                                                    <Col>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Lactose Intolerant"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Lactose Intolerant</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                            <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Vegans"
                                                                    onChange={this.onChange}
                                                                />  <span className="check-fix">Vegans</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                            <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Nuts"
                                                                    onChange={this.onChange}
                                                                />  <span className="check-fix">Nuts</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                            <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Wheat"
                                                                    onChange={this.onChange}
                                                            />  <span className="check-fix">Wheat</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input type="checkbox" /> <span className="check-fix">Corn</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Soy"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Soy</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Garlic"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Garlic</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="user_diets" 
                                                                    value="Gluten"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Gluten</span>
                                                            </Label>
                                                        </FormGroup>
                                                    </Col>
                                                </Row><br />
                                                {/* <FormGroup>
                                                <label>Avatar:</label>
                                                    {this.state.isUploading &&
                                                        <p>Progress: {this.state.progress}</p>
                                                    }
                                                    {this.state.avatarURL &&
                                                        <img src={this.state.avatarURL} />
                                                    }
                                                    <FileUploader
                                                        accept="image/*"
                                                        name="avatar"
                                                        randomizeFilename
                                                        storageRef={firebase.storage().ref('images')}
                                                        onUploadStart={this.handleUploadStart}
                                                        onUploadError={this.handleUploadError}
                                                        onUploadSuccess={this.handleUploadSuccess}
                                                        onProgress={this.handleProgress}
                                                    />
                                                </FormGroup> */}
                                                <Row>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="password">Password&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="password" 
                                                                name="password1" 
                                                                id="password1" 
                                                                placeholder="Enter password..." 
                                                                value={password1}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="confirm_password">Confirm Password&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="password" 
                                                                name="password2" 
                                                                id="password2" 
                                                                placeholder="confirm password..." 
                                                                value={password2}
                                                                onChange={this.onChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                {
                                                    this.state.indiv_error ? <p className="errMsg">Please fill in all the required information</p> : <p></p>
                                                }
                                                {
                                                    this.state.indiv_authError ? <p className="errMsg">Error creating user account. Please verify your information and try again</p> : <p></p>
                                                }
                                                <Row className="testStyle">
                                                    <button className="submit-button" type="submit">Register</button>
                                                </Row>
                                            </Form>
                                        </Col>
                                    </Row>
                                </TabPane>
                                <TabPane tabId="2">
                                    <Row>
                                    <Col sm="12">
                                            <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                                                <Row>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="company_name">Company Name&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="text" 
                                                                name="company_name" 
                                                                id="company_name" 
                                                                value={this.state.company_name}
                                                                onChange={this.onChange}
                                                                placeholder="Enter company name..."
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="company_display_name">Display Name</Label>
                                                            <Input 
                                                                type="text" 
                                                                name="company_display_name" 
                                                                id="company_display_name" 
                                                                value={this.state.company_display_name}
                                                                onChange={this.onChange}
                                                                placeholder="Enter company display name..." 
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <FormGroup>
                                                    <Label htmlFor="company_email">Company Email&nbsp;<span className="errMsg">*</span></Label>
                                                    <Input 
                                                        type="email" 
                                                        name="company_email" 
                                                        id="company_email"
                                                        value={this.state.company_email}
                                                        onChange={this.onChange} 
                                                        placeholder="Enter company email..." 
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label htmlFor="company_email">Company Phone Number&nbsp;<span className="errMsg">*</span></Label>
                                                    <Input 
                                                        type="number" 
                                                        name="company_phone" 
                                                        id="company_phone"
                                                        value={this.state.company_phone}
                                                        onChange={this.onChange} 
                                                        placeholder="Enter company phone number..." 
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label htmlFor="company_location">Company Location&nbsp;<span className="errMsg">*</span></Label>
                                                    <Input 
                                                        type="text" 
                                                        name="company_location" 
                                                        id="company_location"
                                                        value={this.state.company_location}
                                                        onChange={this.onChange}  
                                                        placeholder="Enter company location..." 
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label htmlFor="description">Company Description&nbsp;<span className="errMsg">*</span></Label>
                                                    <Input 
                                                        style={{ borderRadius: 0, borderColor: '#646464', borderWidth: 1 }} 
                                                        type="textarea" 
                                                        name="company_description" 
                                                        id="company_description" 
                                                        value={this.state.company_description}
                                                        onChange={this.onChange} 
                                                        placeholder="Enter company description..." 
                                                    />
                                                </FormGroup>
                                                <Label htmlFor="user_diets">Allergies you support</Label>
                                                <Row>
                                                    <Col>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Lactose Intolerant"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Lactose Intolerant</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                            <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Vegans"
                                                                    onChange={this.onChange}
                                                                />  <span className="check-fix">Vegans</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                            <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Nuts"
                                                                    onChange={this.onChange}
                                                                />  <span className="check-fix">Nuts</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                            <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Wheat"
                                                                    onChange={this.onChange}
                                                            />  <span className="check-fix">Wheat</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input type="checkbox" /> <span className="check-fix">Corn</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Soy"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Soy</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Garlic"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Garlic</span>
                                                            </Label>
                                                        </FormGroup>
                                                        <FormGroup check inline>
                                                            <Label check>
                                                                <Input 
                                                                    type="checkbox"
                                                                    name="company_allergies" 
                                                                    value="Gluten"
                                                                    onChange={this.onChange}
                                                                /> <span className="check-fix">Gluten</span>
                                                            </Label>
                                                        </FormGroup>
                                                    </Col>
                                                </Row><br />
                                                <Row>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="company_password1">Password&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="password" 
                                                                name="company_password1" 
                                                                id="company_password1" 
                                                                value={this.state.company_password1}
                                                                onChange={this.onChange} 
                                                                placeholder="Enter password..." 
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="company_password2">Confirm Password&nbsp;<span className="errMsg">*</span></Label>
                                                            <Input 
                                                                type="password" 
                                                                name="company_password2" 
                                                                id="company_password2" 
                                                                value={this.state.company_password2}
                                                                onChange={this.onChange} 
                                                                placeholder="Confirm password..." 
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="facebook">Facebook Link</Label>
                                                            <Input 
                                                                type="text" 
                                                                name="facebook" 
                                                                id="facebook" 
                                                                value={this.state.facebook}
                                                                onChange={this.onChange} 
                                                                placeholder="Facebook URL" 
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12" md="6" lg="6">
                                                        <FormGroup>
                                                            <Label htmlFor="twitter">Twitter Link</Label>
                                                            <Input 
                                                                type="text" 
                                                                name="twitter" 
                                                                id="twitter" 
                                                                value={this.state.twitter}
                                                                onChange={this.onChange} 
                                                                placeholder="Twitter URL" 
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <FormGroup>
                                                    <Label htmlFor="other_links">Other Links</Label>
                                                    <Input 
                                                        type="text" 
                                                        name="other_links" 
                                                        id="other_links" 
                                                        value={this.state.other_links}
                                                        onChange={this.onChange} 
                                                        placeholder="Other URLs" 
                                                    />
                                                </FormGroup>
                                                {
                                                    this.state.comp_error ? <p className="errMsg">Please fill in all the required information</p> : <p></p>
                                                }
                                                {
                                                    this.state.comp_authError ? <p className="errMsg">Error creating food business. Please verify your internet connection and try again</p> : <p></p>
                                                }
                                                <Row className="testStyle">
                                                    <button className="submit-button" type="submit">Register</button>
                                                </Row>
                                            </Form>
                                        </Col>
                                    </Row>
                                </TabPane>
                            </TabContent>
                        </CardBody>
                    </Card>
                </Container>
                <Footer />
            </div>
        )
    }
}

export default connect(null, { createAccount, padUserData, createFoodBusiness })(Register);