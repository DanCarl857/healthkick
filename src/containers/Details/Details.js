import React, { Component } from 'react';
import {
    Container,
    Col,
    Row,
    Input,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Button
} from 'reactstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { 
    getSingleFoodBusiness, 
    updateFoodBusinessWithReviews,
    updateFoodBusinessWithOrder,
    closeBusiness,
    getMenuItems,
    deleteFoodBusiness
} from './../../actions/FoodBusinessesAction';
import classnames from 'classnames';
import _ from 'lodash';
import moment from 'moment';

// styles
import './Details.css';

// components
import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';
import Footer from './../../components/Footer';

import { getUserData } from './../../actions/UserActions';

class Details extends Component {

    componentWillMount() {
        const { match, getSingleFoodBusiness, getMenuItems } = this.props;
        getSingleFoodBusiness(match.params.id);
        getMenuItems(match.params.id);
    }

    constructor(props) {
        super(props);

        this.state = {
            modal: false,
            orderModal: false,
            stars: '',
            review: '',
            closed: false,
            orderCount: 1,
            unitPrice: 0,
            total: 0,
            order_name: '',
            isAdmin: null,
            visible: false,
            orderError: false,
            reviewError: false,
            reviewAuthErr: false,
            future: true,
            order_date: '',
            order_time: '',
            showAlert: false
        }

        this.toggle = this.toggle.bind(this);
        this.toggleOrder = this.toggleOrder.bind(this);
        this.submitReview = this.submitReview.bind(this);
        this.onChange = this.onChange.bind(this);
        this.closeBusiness = this.closeBusiness.bind(this);
        this.placeOrder = this.placeOrder.bind(this);
    }

    componentDidMount() {
        const { user_data } = this.props;
        const dbKey = Object.keys(user_data);
        var elem = dbKey[0];
        if(_.has(user_data[elem], 'isAdmin')) {
            var value = user_data[elem]['isAdmin'];
            this.setState({ isAdmin: value });
        }
    }

    submitReview(e) {
        e.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { stars, review } = this.state;

        if (stars === '' || review === '') {
            this.setState({ visible: false, reviewError: true });
            return;
        }

        var data = {
            stars: stars,
            review: review
        };
        this.props.updateFoodBusinessWithReviews(this.props.match.params.id, data);
    }

    closeBusiness() {
        this.props.closeBusiness(this.props.match.params.id, this.state.closed);
        this.setState({ closed: !this.state.closed });
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    placeOrder(e) {
        this.setState({ visible: !this.state.visible });
        var total = this.state.total;
        if (total === 0) {
            total = this.state.unitPrice * this.state.orderCount;
        }
        var datus = '';
        var timus = '';

        // Date formatting
        if(this.state.order_date === '') {
            var currentDate = new Date();
            var format_date = moment(currentDate).format('YYY-MM-DD');
            datus = format_date.substring(2);
        } else {
            datus = this.state.order_date;
        }

        // Time formatting
        if(this.state.order_time === '') {
            var currentTime = new Date();
            var format_time = moment(currentTime).format('HH:mm');
            timus = format_time;
        } else {
            timus = this.state.order_time;
        }

        var order = {
            number_of_orders: this.state.orderCount,
            unit_price: this.state.unitPrice,
            total,
            name: this.state.order_name,
            company_name: this.props.foody.company_name,
            date_of_order: datus,
            time_of_order: timus,
            status: false
        }

        // this.props.updateFoodBusinessWithOrder(this.props.match.params.id, order);
        new Promise((resolve) => resolve(this.props.updateFoodBusinessWithOrder(this.props.match.params.id, order)))
            .then(() => {

                // get user data values
                let values = Object.values(this.props.user_data)[0];
                
                // send email to food business here...
                fetch('https://api.emailjs.com/api/v1.0/email/send', {
                    headers: {
                      'Content-type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify({
                        service_id: 'gmail',
                        template_id: 'healthkick',
                        user_id: 'user_gQ7ea2Nw3SdJgAO2wpXF8',
                        template_params: {
                            from_name: 'Healthkick',
                            send_email: this.props.foody.company_email,
                            to_name: this.props.foody.company_name,
                            message_html: `<p>You just received an order: </p><p><b>User Phone: </b>${values.phone}</p><p><b>Menu Item:</b>${order.name}</p><p><b>Number of Orders: </b>${order.number_of_orders}</p><p><b>Unit Price: </b>${order.unit_price}</p><p><b>Total Price: </b>${order.total}</p><p><b>Date of Order: ${order.date_of_order}</b></p><p><b>Time of delivery: </b>${order.time_of_order}</p><p>Kindly fulfill this delivery.</p><br />Cheers!`
                        }
                    })
                  })
                  .then(() => this.toggleOrder()) // close modal on successfull send of email
                  .catch(err => console.log(err)); // TODO: Handle this properly
            });
    }

    toggle() { this.setState({ modal: !this.state.modal }); }

    toggleOrder(key, unitPrice, order_name) { 
        if (this.state.orderModal) {
            this.setState({
                orderCount: 1,
                total: 0,
                unitPrice: 0,
                order_name: ''
            });
        }
        this.setState({ orderModal: !this.state.orderModal, unitPrice, order_name  }); 
    }

    add = () => {
        var total = (this.state.orderCount+1) * this.state.unitPrice;
        this.setState(prevState => { return { orderCount: prevState.orderCount + 1, total }});
    }

    subtract = () => { 
        if ((this.state.orderCount-1) >= 1) {
            var total = (this.state.orderCount-1) * this.state.unitPrice;
            this.setState(prevState => { return { orderCount: prevState.orderCount - 1, total }});
        }
    };

    renderTotalPrice = () => {
        if(this.state.total === 0) {
            return <div>Total: $ {this.state.unitPrice}</div>
        } else {
            return <div>Total: $ {this.state.total}</div>
        }
    }

    deleteItem(id) {
        new Promise(resolve => resolve(this.props.deleteFoodBusiness(this.props.match.params.id, id)))
            .then(() => {
                this.setState({ showAlert: true });
                setTimeout(() => this.setState({ showAlert: false }), 8000);
            });
    }

    render() {
        const { foody } = this.props;
        
        let values = {}
        if(foody.toString() !== [].toString()){
            values = Object.values(this.props.foody)[0];
        } else {
            values = [];
        }

        return (
            <div>
                {
                    this.state.isAdmin ? <BusinessHeader /> : <UserHeader />
                }
                <Container>
                    <Row>
                        <Col xs="12" sm="12" md="6" lg="6">
                            <h1 className="details-header">>&nbsp;
                                {
                                    values.display_name ? values.display_name : values.company_name
                                }
                            </h1>
                        </Col>
                        {
                            this.state.isAdmin ? 
                            <Col xs="12" sm="12" md="6" lg="6">
                                <Button 
                                    className={classnames('float-right', { closedbutton: !this.state.closed, openbutton: this.state.closed })}
                                    onClick={this.closeBusiness}
                                >
                                    {
                                        !this.state.closed ? 'Close Business' : 'Open Business'
                                    }
                                </Button>
                            </Col> : <Col></Col>
                        }
                    </Row>
                    <Row>
                        <Col xs="12" md="6" lg="6" className="container-img">
                            <img className="cover-image" src={require('./../../assets/rest.jpeg')} alt="cover" />
                        </Col>
                        <Col xs="12" md="6" lg="6">
                            <h3 className="overview-header">Description</h3>
                            <p className="overview-text">
                                {values.description}
                            </p>
                            <hr/>
                            <h4>Location</h4>
                            <p>{values.location}</p>

                            <h4>Social Media</h4>
                            <Row>
                                <Col xs="4" sm="4" md="4" lg="4">
                                    {
                                        values.facebook_link ? <a href={values.facebook_link} target="_blank">Facebook</a> : <p className="infoMsg">None</p>
                                    }
                                </Col>
                                <Col xs="4" sm="4" md="4" lg="4">
                                    {
                                        values.twitter_link ? <a href={values.twitter_link} target="_blank">Twitter</a> : <p className="infoMsg">None</p>
                                    }
                                </Col>
                                <Col xs="4" sm="4" md="4" lg="4">
                                    {
                                        values.other_links ? <a href={values.other_links} target="_blank">Others</a> : <p></p>
                                    }
                                </Col>
                            </Row>
                            <hr/>
                            <Row>
                                <Col xs="6" md="9" lg="9">
                                    <h4>Reviews</h4>
                                </Col>
                                <Col xs="6" md="3" lg="3">
                                    <button className="rating-btn" onClick={this.toggle}>Review</button>
                                    <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal-top">
                                        <ModalHeader toggle={this.toggle}>Give a review</ModalHeader>
                                        <ModalBody className="rev-body">
                                            <Input type="number" name="stars" value={this.state.stars} onChange={this.onChange} placeholder="Number of stars/5*" />
                                            <br />
                                            <Input type="textarea" name="review" value={this.state.review} onChange={this.onChange} placeholder="Enter your review*" />
                                        </ModalBody>
                                        {
                                            this.state.reviewError ? <p className="errMsg">Please fill in all the required information</p> : <p></p>
                                        }
                                        {
                                            this.state.reviewAuthErr ? <p className="errMsg">Error submitting review. Please verify your internet connection and try again</p> : <p></p>
                                        }
                                        <ModalFooter className="rev-foot">
                                            <Button style={{ color: '#fff', backgroundColor: '#ff7979', borderWidth: 1, borderColor: '#ff7979' }} onClick={this.submitReview}>Submit</Button>{' '}
                                            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                        </ModalFooter>
                                    </Modal>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="6" md="9" lg="9">
                                    8.0/10 rating
                                </Col>
                                <Col xs="6" md="3" lg="3">
                                    <div>(1200 reviews)</div>
                                </Col>
                            </Row>
                        </Col>
                    </Row><br /><br />
                    <Row className="search-filter">
                        <Col xs="12" md="9" lg="9">
                            <Input type="text" placeholder="Search by menu items..." />
                            <br/>
                        </Col>
                        
                        <Col xs="12" md="3" lg="3">
                            <button className="search-btn">Search</button>
                            <br/>
                        </Col>
                    </Row>
                    <Row className="marginTop70">
                        {
                            this.state.showAlert ? 
                            <div className="centerStyle alertStyle">
                                Succesfully deleted menu item
                            </div> : <div></div>
                        }
                    </Row>
                    <Row className="marginTop70">
                        {
                            _.map(this.props.menu_items, (menu_item, key) => {
                                return (
                                    <Col xs="12" sm="12" md="3" lg="3" className="menu-item" key={key}>
                                        <img src={require('./../../assets/rest1.jpeg')} alt="Restaurant 1" />
                                        <p className="food-text">{menu_item.name}</p>
                                        <p className="reviews">
                                            {menu_item.description}
                                        </p>
                                        <p className="center-it">
                                            <span className="price">
                                                Price: $ {menu_item.price}
                                            </span>
                                        </p>
                                        <div>
                                            {
                                                this.state.isAdmin ?
                                                <Row className="center-it">
                                                    <Col xs={12} sm={12} md={6}>
                                                        <button className="delete-btn" onClick={() => this.deleteItem(key)}>Delete</button>&nbsp;&nbsp;&nbsp;
                                                    </Col>
                                                    <Col xs={12} sm={12} md={6}>
                                                        <button className="order-btn" onClick={() => this.toggleOrder(key, menu_item.price, menu_item.name)}>Order</button>
                                                    </Col>
                                                </Row> :
                                                <p className="center-it">
                                                    <button className="order-btn" onClick={() => this.toggleOrder(key, menu_item.price, menu_item.name)}>Order</button>
                                                </p>
                                            }
                                        </div>
                                    </Col>
                                )
                            })
                        }
                    </Row>
                    <Modal isOpen={this.state.orderModal} toggle={this.toggleOrder} className="modal-top">
                        <ModalHeader toggle={this.toggleOrder}>Place Order</ModalHeader>
                        <ModalBody className="rev-body">
                            <Container className="center-it">
                                <Row>
                                    <Col xs="12" md="4" lg="4" className="operand" onClick={this.subtract}>
                                        -
                                    </Col>
                                    <Col xs="12" md="4" lg="4" className="qty">
                                        {this.state.orderCount}
                                    </Col>
                                    <Col xs="12" md="4" lg="4" className="operand" onClick={this.add}>
                                        +
                                    </Col>
                                </Row><br/>
                                <Row className="center-it">
                                    <Col xs="12" md="12" lg="12" className="unit">
                                        Unit Price: $ { this.state.unitPrice }
                                    </Col>
                                </Row><br />
                                <Row className="center-it total">
                                    <Col xs="12" md="12" lg="12">
                                        { this.renderTotalPrice() }
                                    </Col>
                                </Row>
                                <div className={classnames('center-it', { displayNone: this.state.future })}>
                                    <Input type="date" name="order_date" id="date" value={this.state.order_date} onChange={this.onChange} placeholder="Enter date of order" /><br />
                                    <Input type="time" name="order_time" id="time" value={this.state.order_time} onChange={this.onChange} placeholder="Enter time of order" />
                                </div>
                                {
                                    this.state.orderError ? <p className="errMsg">Error placing order. Please check your internet connection and try again</p> : <p></p>
                                }
                            </Container>
                        </ModalBody>
                        <ModalFooter className="rev-foot">
                            <Button className="float-left" style={{ color: '#fff', backgroundColor: '#14cf14', borderWidth: 1, borderColor: '#14cf14' }} onClick={() => this.setState({ future: false })}>Future Order</Button>{' '}
                            <Button style={{ color: '#fff', backgroundColor: '#ff7979', borderWidth: 1, borderColor: '#ff7979' }} onClick={this.placeOrder}>Submit</Button>{' '}
                            <Button color="secondary" onClick={this.toggleOrder}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </Container><br/><br/>
                <Footer />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        foody: state.singleFoodBusiness,
        menu_items: state.menuItems,
        user_data: state.userData,
    };
}

export default connect(mapStateToProps, { 
    getSingleFoodBusiness, 
    updateFoodBusinessWithReviews,
    updateFoodBusinessWithOrder,
    closeBusiness,
    getMenuItems,
    getUserData,
    deleteFoodBusiness
})(Details);