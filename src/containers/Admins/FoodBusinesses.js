//import liraries
import React, { Component } from 'react';
import { 
    Table, 
    Container, 
    Row, Col, 
    Modal, ModalBody, ModalFooter, ModalHeader, 
    Input,
    Button,
    Form, FormGroup, Label
} from 'reactstrap';
import { connect } from 'react-redux';
import { getFoodBusinesses, createFoodBusiness } from './../../actions/FoodBusinessesAction';
import _ from 'lodash';
import './styles.css';

import BusinessHeader from './../../components/BusinessHeader';

// create a component
class FoodBusinesses extends Component {

    componentWillMount() {
        this.props.getFoodBusinesses();
    }

    componentDidMount() {

        console.log(this.props.foods);
    }

    constructor(props) {
        super(props);

        this.state = {
            modal: false,
            company_name: '',
            display_name: '',
            company_email: '',
            location: '',
            password1: '',
            password2: '',
            facebook_link: '',
            twitter_link: '',
            other_links: '',
            description: '',
            menu_items: [],
            orders: [],
            reviews: [],
            isAdmin: false,
            visible: false, 
            error: false,
            authError: false
        }

        this.renderTableRow = this.renderTableRow.bind(this);
        this.toggle = this.toggle.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    toggle() { this.setState({ modal: !this.state.modal }); }

    onChange(event) { this.setState({ [event.target.name]: event.target.value }); }

    onSubmit(event) {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });

        const { company_email, company_name, password1, password2, location, description, display_name } = this.state;

        if (company_email === '' || company_name === '' || password1 === '' || password2 === '' || location === '' || description === '') {
            this.setState({ visible: false, error: true });
            return;
        }

        this.props.createFoodBusiness(this.state).then(() => {
            this.setState({ visible: false });
        }).catch(err => {
            this.setState({ visible: false, authError: true });
        });
    }

    renderTableRow() {
        return _.map(this.props.foods, (food, key) => {
            return (
                <tr key={key}>
                    <td>{food.company_name}</td>
                    <td>{food.company_email}</td>
                    <td>{food.location}</td>
                    { food.open ? <td className="open">Open</td> : <td className="closed">Closed</td> }
                    { food.menu_items ? <td>Yes</td> : <td>None</td> }
                    { food.reviews ? <td>Yes</td> : <td>None</td> }
                    <td>{food.facebook_link}</td>
                    <td>{food.twitter_link}</td>
                    <td>{food.other_links}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <BusinessHeader />
                <Container className="marginTop90">
                    <Row>
                        <Col xs="12" sm="12" md="6" lg="6">
                            <h3>All Food Businesses</h3>
                        </Col>
                        <Col xs="12" sm="12" md="6" lg="6" className="float-right">
                            <div className="add float-right" onClick={this.toggle}>
                                + Add Food Business
                            </div>
                        </Col>
                        <Modal isOpen={this.state.modal} toggle={this.toggle}>
                            <ModalHeader toggle={this.toggle}>Create a Food Business</ModalHeader>
                            <ModalBody className="rev-body">
                            <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="company_name">Company Name&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="text" 
                                                    name="company_name" 
                                                    id="company_name" 
                                                    placeholder="Enter company name..."
                                                    value={this.state.company_name}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="display_name">Display Name</Label>
                                                <Input 
                                                    type="text" 
                                                    name="display_name" 
                                                    id="display_name" 
                                                    placeholder="Enter company display name..."
                                                    value={this.state.display_name}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <FormGroup>
                                        <Label htmlFor="company_email">Company Email&nbsp;<span className="errMsg">*</span></Label>
                                        <Input 
                                            type="email" 
                                            name="company_email" 
                                            id="company_email" 
                                            placeholder="Enter company email..." 
                                            value={this.state.company_email}
                                            onChange={this.onChange}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label htmlFor="description">Description&nbsp;<span className="errMsg">*</span></Label>
                                        <Input 
                                            style={{ borderRadius: 0, borderColor: '#646464', borderWidth: 1 }}
                                            type="textarea" 
                                            name="description" 
                                            id="description" 
                                            placeholder="Enter company description..." 
                                            value={this.state.description}
                                            onChange={this.onChange}
                                        />
                                    </FormGroup>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="password1">Password&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="password" 
                                                    name="password1" 
                                                    id="password1" 
                                                    placeholder="Enter password..." 
                                                    value={this.state.password1}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="password2">Confirm Password&nbsp;<span className="errMsg">*</span></Label>
                                                <Input 
                                                    type="password" 
                                                    name="password2" 
                                                    id="password2" 
                                                    placeholder="Confirm password..." 
                                                    value={this.state.password2}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <FormGroup>
                                        <Label htmlFor="location">Location&nbsp;<span className="errMsg">*</span></Label>
                                        <Input 
                                            type="text" 
                                            name="location" 
                                            id="location" 
                                            placeholder="Enter location..." 
                                            value={this.state.company_email}
                                            onChange={this.onChange}
                                        />
                                    </FormGroup>
                                    <Row>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="facebook-link">Facebook Link</Label>
                                                <Input 
                                                    type="text" 
                                                    name="facebook_link" 
                                                    id="facebook_link" 
                                                    placeholder="Facebook url..." 
                                                    value={this.state.facebook_link}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6" lg="6">
                                            <FormGroup>
                                                <Label htmlFor="twitter-link">Twitter Link</Label>
                                                <Input 
                                                    type="text" 
                                                    name="twitter_link" 
                                                    id="twitter_link" 
                                                    placeholder="Twitter url..." 
                                                    value={this.state.twitter_link}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <FormGroup>
                                        <Label htmlFor="other_links-link">Other Links</Label>
                                        <Input 
                                            type="text" 
                                            name="other_links" 
                                            id="other_links" 
                                            placeholder="Other urls..." 
                                            value={this.state.other_links}
                                            onChange={this.onChange}
                                        />
                                    </FormGroup>
                                    {
                                        this.state.error ? <p className="errMsg">Please fill in all required information</p> : <p></p>
                                    }
                                    {
                                        this.state.authError ? <p className="errMsg">Error creating food business. Verify you internet connection and try again</p> : <p></p>
                                    }
                                    <Row className="testStyle">
                                        <button type="submit" className="submit-button">Submit</button>
                                    </Row>
                                </Form>
                            </ModalBody>
                            {
                                this.state.reviewError ? <p className="errMsg">Please fill in all the required information</p> : <p></p>
                            }
                            {
                                this.state.reviewAuthErr ? <p className="errMsg">Error submitting review. Please verify your internet connection and try again</p> : <p></p>
                            }
                            <ModalFooter className="rev-foot">
                                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </Row>
                    <Table responsive hover>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Location</th>
                            <th>Open</th>
                            <th>Menu Items?</th>
                            <th>Reviews?</th>
                            <th>Facebook Link</th>
                            <th>Twitter Link </th>
                            <th>Other Links</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.renderTableRow()}
                        </tbody>
                    </Table>
                </Container>
                {/* <Footer /> */}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        foods: state.foodBusinesses,
        user_data: state.userData
    }
} 

//make this component available to the app
export default connect(mapStateToProps, { getFoodBusinesses, createFoodBusiness })(FoodBusinesses);
