//import liraries
import React, { Component } from 'react';
import { Table, Container } from 'reactstrap';

import BusinessHeader from './../../components/BusinessHeader';

// create a component
class Orders extends Component {
    render() {
        return (
            <div>
                <BusinessHeader />
                <Container className="marginTop90 marginBottom90">
                    <h3>Orders</h3><br />
                    <Table responsive hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>User</th>
                            <th>Date of order</th>
                            <th>Date of delivery</th>
                            <th># of orders</th>
                            <th>Unit Price</th>
                            <th>Total Price</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </Table>
                </Container>
                {/* <Footer /> */}
            </div>
        );
    }
}

//make this component available to the app
export default Orders;
