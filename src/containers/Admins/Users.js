//import liraries
import React, { Component } from 'react';
import { Table, Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Link } from 'react-router-dom';

import BusinessHeader from './../../components/BusinessHeader';
import Footer from './../../components/Footer';
import './styles.css';

import { getAllUsers } from './../../actions/UserActions';

// create a component
class Users extends Component {

    componentWillMount() {
        this.props.getAllUsers();
    }

    componentDidMount() {
        console.log(this.props.users);
    }

    constructor(props) {
        super(props);

        this.state = {
            modal: false
        }

        this.renderTableRow = this.renderTableRow.bind(this);
    }

    renderTableRow() {
        return _.map(this.props.users, (user, key) => {
            return (
                <tr key={key}>
                    <td>{user.name}</td>
                    <td>{user.username}</td>
                    <td>{user.email}</td>
                    { user.isAdmin ? <td className="open">Yes</td> : <td className="closed">No</td> }
                    <td>
                        <Row>
                            <Col xs="12" sm="12" md="6" lg="6">
                                <Row>
                                    <div className="edit">
                                        <img width="18" height="18" src={require('./../../assets/edit.png')} />
                                        &nbsp;Switch
                                    </div>
                                </Row>
                            </Col>
                            <Col xs="12" sm="12" md="6" lg="6">
                                <Row className="delete">
                                    <div className="delete">
                                        <img width="16" height="16" src={require('./../../assets/delete.png')} />
                                        &nbsp;Delete
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <BusinessHeader />
                <Container className="marginTop90">
                    <Row>
                        <Col xs="12" sm="12" md="6" lg="6">
                            <h3>All Users</h3>
                        </Col>
                        <Col xs="12" sm="12" md="6" lg="6" className="float-right">
                            <Link to="/register">
                                <div className="add float-right" onClick={this.toggle}>
                                    + Add Users
                                </div>
                            </Link>
                        </Col>
                    </Row>
                    <Table responsive hover>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Is Admin?</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.renderTableRow()}
                        </tbody>
                    </Table>
                </Container>
                {/* <Footer /> */}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.users
    }
}

//make this component available to the app
export default connect(mapStateToProps, { getAllUsers })(Users);
