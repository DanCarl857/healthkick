//import liraries
import React, { Component } from 'react';
import {
    Container,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Input,
    Button,
    Table
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getUser, getUserData } from './../../actions/UserActions';
import { getOrders } from './../../actions/FoodBusinessesAction';

// import components
import UserHeader from './../../components/UserHeader';
import BusinessHeader from './../../components/BusinessHeader';
import Footer from './../../components/Footer';

// create a component
class Order extends Component {

    componentWillMount() {
        this.props.getUser();

        if (this.props.user.loading === false && this.props.user.email === undefined) {
            this.props.history.replace('/login');
        } else {
            this.props.getUserData(this.props.user.email);
            this.props.getOrders()
        }
    }

    constructor(props) {
        super(props);

        this.state = { isAdmin: false };
    }

    componentDidMount() {
        console.log(this.props.orders);
        const { user_data } = this.props;
        console.log(user_data);
        const dbKey = Object.keys(user_data);
        var elem = dbKey[0];
        if(_.has(user_data[elem], 'isAdmin')) {
            var value = user_data[elem]['isAdmin'];
            this.setState({ isAdmin: value });
        }
    }

    render() {
        return (
            <div>
                {
                    this.state.isAdmin ? <BusinessHeader /> : <UserHeader />
                }
                <Container className="marginTop90">
                    <h3>Orders</h3><br />
                    <Table responsive hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>User</th>
                            <th>Date of order</th>
                            <th>Date of delivery</th>
                            <th># of orders</th>
                            <th>Unit Price</th>
                            <th>Total Price</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </Table>
                </Container>
                {
                    this.state.isAdmin ? 
                    <Link to="/create-business">
                        <div className="fab"> + </div>
                    </Link> : <div></div>
                }
                <div className="fab-suggestions" onClick={this.toggle}> ? </div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal-top">
                    <ModalHeader toggle={this.toggle}>Make A Suggestion to the Administrator(s)</ModalHeader>
                    <ModalBody className="rev-body">
                        <Input type="text" name="title" value={this.state.value} onChange={this.onChange} placeholder="Enter title..." />
                        <br />
                        <Input style={{ borderRadius: 0 }} type="textarea"  name="suggestion" value={this.state.suggestion} onChange={this.onChange} placeholder="Enter your suggestion..." />
                    </ModalBody>
                    <ModalFooter className="rev-foot">
                        <Button style={{ color: '#fff', backgroundColor: '#ff7979', borderWidth: 1, borderColor: '#ff7979' }} onClick={this.submitSuggestion}>Submit</Button>{' '}
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <br/><br/>
                <div className="marginTop110">
                    <Footer />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps){
    return {
        foods: state.foodBusinesses,
        user: state.user,
        user_data: state.userData,
        orders: state.orders
    }
}


//make this component available to the app
export default connect(mapStateToProps, { getUser, getUserData, getOrders })(Order);
