import React, { Component } from 'react';
import './Login.css';
import {
    Container,
    Row,
    Col,
    Card, 
    CardTitle, 
    CardBody,
    FormGroup, Form, Input, Label
} from 'reactstrap';
import HeaderRegistration from '../../components/HeaderRegistration';
import Footer from '../../components/Footer';
import { login, getUser, resetPasswordWithEmail } from './../../actions/UserActions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Login extends Component {

    componentWillMount() {
        this.props.getUser();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.email !== undefined) {
            this.props.history.push('/dashboard');
        }
    }
    
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            error: false,
            visible: false,
            authError: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ visible: !this.state.visible });
        const { email, password } = this.state;

        if (email === '' || password === '') {
            this.clearFields();
            this.setState({ error: true, visible: false });
            return;
        }

        this.props.login(email, password).catch(err => {
            this.clearFields();
            this.setState({ authError: true, visible: false });
            return;
        });
    }

    clearFields() {
        this.setState({ email: '', password: '', error: false, authError: false });
    }

    render() {
        return (
            <div className="registration-bg">
                <HeaderRegistration question="Don't have an Account?" actionText="Signup" myLink="/register" />
                <Container className="container-register center-block">
                    <Card className="container-form">
                        <CardTitle className="centered-text register-header">Login</CardTitle>
                        <CardBody>
                        <Row>
                            <Col sm="12">
                                <Form className="indiv-reg-form" onSubmit={this.onSubmit}>
                                    <FormGroup>
                                        <Label for="email">Email</Label>
                                        <Input 
                                            type="email" 
                                            name="email" 
                                            id="email" 
                                            value={this.state.email}
                                            onChange={this.onChange}
                                            placeholder="Enter email" 
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="password">Password</Label>
                                        <Input 
                                            type="password" 
                                            name="password" 
                                            id="password" 
                                            value={this.state.password}
                                            onChange={this.onChange}
                                            placeholder="Enter password" 
                                        />
                                    </FormGroup>
                                    {
                                        this.state.error ? <p className="errMsg">Please fill in both username and password</p> : <p></p>
                                    }
                                    {
                                        this.state.authError ? <p className="errMsg">Error logging in. Please verify your information and try again</p> : <p></p>
                                    }
                                    <Row className="testStyle">
                                        <button className="submit-button">Login</button>
                                    </Row>
                                    <Row className="testStyle">
                                        <Link to="/forgotten-password">
                                            <div style={{ marginTop: 15, color: '#3e4852' }}>Forgotten Password?</div>
                                        </Link>
                                    </Row>
                                </Form>
                            </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Container>
                <div className="marginTop110">
                    <Footer />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { user: state.user };
};

export default connect(mapStateToProps, { login, getUser, resetPasswordWithEmail })(Login);