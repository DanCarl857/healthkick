import React, { Component } from 'react';
import './styles.css';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { logout } from './../actions/UserActions';
import { Link } from 'react-router-dom';

class BusinessHeader extends Component {

    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout() {
        this.props.logout();
        // this.props.history.replace('/');
    }

    render() {
        return (
            <div className="header">
                <div className="header-content">
                    <Row>
                        <Col xs="12" sm="12" md="4" className="logo-head">
                            <Link to="/" style={{ color: '#ff7979' }}>HEALTHKICK</Link>
                        </Col>
                        <Col xs="12" sm="12" md="3"></Col>
                        <Col xs="12" sm="12" md="5" className="login-button float-right">
                            <Row>
                                <Col xs="3" md="2" lg="2" className="user-head-icons">
                                    <Link to="/dashboard">
                                        <div className="already">
                                            <img width="25" height="25" src={require('./../assets/home.png')} alt="home" /><br />
                                            <p className="already">Home</p>
                                        </div>
                                    </Link>
                                </Col>
                                <Col xs="3" md="2" lg="2" className="user-head-icons">
                                    <Link to="/admin">
                                        <div className="already">
                                            <img width="25" height="25" src={require('./../assets/admin_orders.png')} alt="home" /><br />
                                            <p className="already">Dashboard</p>
                                        </div>
                                    </Link>
                                </Col>
                                <Col xs="3" md="2" lg="2" onClick={this.logout}>
                                    <Link to="/">
                                        <img width="25" height="25" src={require('./../assets/logout.png')} alt="logout" /><br />
                                        <p className="already">Logout</p>
                                    </Link>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

export default connect(null, { logout })(BusinessHeader);
