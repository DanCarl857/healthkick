import React, { Component } from 'react';
import './styles.css';
import {
    Container,
    Row,
    Col,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom';

class Footer extends Component {
    render() {
        return (
            <div className="footer-section">
                <Container>
                    <Row>
                        <h2 className="footer-main-header">HealthKick</h2>
                    </Row>
                    <Row>
                        <Col xs="12" md="4" lg="4">
                            <Row>
                                <Col xs="12" md="6" lg="6">
                                    <h5 className="footer-headers">CONTENT</h5>
                                    <ul className="list-style">
                                        <li>
                                            <Link to="/dashboard" style={{ color: '#fff' }}>Browse</Link>
                                        </li>
                                        <li>
                                            <Link to="/profile" style={{ color: '#fff' }}>Profile</Link>
                                        </li>
                                        <li>
                                            <Link to="/location" style={{ color: '#fff' }}>Locations</Link>
                                        </li>
                                    </ul>
                                </Col>
                                <Col xs="12" md="6" lg="6">
                                    <h5 className="footer-headers">ABOUT</h5>
                                    <ul className="list-style">
                                        <li><a href="mailto:healthkick@gmail.com" style={{ color: '#fff' }}>healthkick@gmail.com</a></li>
                                        <li>+1 999 XXX XXX</li>
                                        <li>Boston, Ma</li>
                                    </ul>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" md="8" lg="8">
                            
                            <div className="footer-search float-right">
                                <div style={{ marginBottom: 15 }}>Search for Restaurants and Menu Items</div>
                                <Input type="text" placeholder="Enter name..." />
                            </div>
                        </Col>
                    </Row>
                    <hr className="footer-divider"/>
                    <Row>
                        <Col xs="12" md="4" lg="4">
                            <Row>
                                <Col xs="12" md="6" lg="6">
                                    <Link to="/terms-and-conditions" style={{ color: '#fff' }}>Terms & Conditions</Link>
                                </Col>
                                <Col xs="12" md="6" lg="6">
                                    HealthKick &copy; 2018.
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" md="8" lg="8">
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Footer;