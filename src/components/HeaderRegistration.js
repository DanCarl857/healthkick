import React, { Component } from 'react';
import './styles.css';
import { Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

class HeaderRegistration extends Component {
    render() {
        return (
            <div className="header">
                <div className="header-content">
                    <Row>
                        <Col xs="12" sm="12" md="6" className="logo-head">
                            HEALTHKICK
                        </Col>
                        <Col xs="12" sm="12" md="6" className="login-button">
                            <Link to={`${this.props.myLink}`}>
                                <p className="already">{this.props.question} <span className="login-text">{this.props.actionText}</span></p>
                            </Link>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

export default HeaderRegistration;
