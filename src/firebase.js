import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyB_h1jhP2ZzA2YjZN6ajSDXT5QBFDR3JmQ",
    authDomain: "healthkick-1c6c4.firebaseapp.com",
    databaseURL: "https://healthkick-1c6c4.firebaseio.com",
    projectId: "healthkick-1c6c4",
    storageBucket: "healthkick-1c6c4.appspot.com",
    messagingSenderId: "602609331169"
};

firebase.initializeApp(config);

export const database = firebase.database();
export const auth = firebase.auth();